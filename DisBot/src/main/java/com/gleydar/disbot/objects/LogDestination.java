/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */
package com.gleydar.disbot.objects;

/**
 *
 * @author Antimonyy
 */


public enum LogDestination {
    FILE, CHANNEL
}
