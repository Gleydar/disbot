/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.objects;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Antimonyy
 */

public class DisUser{
    

    public static int getPermissionLevel(String ID){
        ArrayList<Map> values =  (ArrayList)ConfigUtil.getObjectValue("users");
        for(Map m : values){
            if(m.containsValue(ID)){
                return Integer.parseInt((String)m.get("permission"));
            }
        }
        return 0;
    }
}
