/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.objects;

import com.gleydar.disbot.events.MessageHandler;
import net.dv8tion.jda.entities.Message;

/**
 *
 * @author Antimonyy
 */

public abstract class Module {
    
    public Module(){
        register();
    }
    
    public final void register(){
        MessageHandler.registerModule(this);
    }
    public final void unregister(){
        MessageHandler.unregisterModule(this);
    }
    public abstract void execute(String[] args, Message m) throws Exception;
    public abstract int getPermissionLevel();
    public abstract String getLabel();
    
    
}
