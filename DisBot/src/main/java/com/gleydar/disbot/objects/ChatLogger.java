/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.objects;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.modules.ChatLoggerHandler;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.entities.Channel;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.User;

/**
 *
 * @author Antimonyy
 */

public class ChatLogger extends MessageConsumer{
    
    private final Channel channel;
    private final User s;
    private boolean shouldStop = false;
    private LogDestination l;
    private Channel logchannel;
    private File f;
    
    /*
    * This one SHOULD start logging at the time of its creation... 
    */
    public ChatLogger(Channel channelname, User sender, LogDestination ld, Channel lchannel){
        channel = channelname;
        logchannel = lchannel;
        l = ld;
        s = sender; 
        ChatLoggerHandler.registerNewLogger(this);
        createFile();
        System.out.println("Started new logger on channel " + channel.getName() + " with ID " + channel.getId());
    }
    
    /*
    * Returns the channel the Bot is logging
    */
    public Channel getChannel(){
        return channel;
    }
    
    public LogDestination getDestination(){
        return l;
    }
    
    private void createFile(){
        if(l.equals(LogDestination.FILE)){
            f = new File(channel.getName() + ".log");
            if(!f.exists()){
                try {
                    f.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(ChatLogger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void stop(){   
        shouldStop = true;
        ChatLoggerHandler.removeLogger(this);
    }

    @Override
    public void handle(Message m) {
        if(!shouldStop){
            if(m.getChannelId().equals(channel.getId())){
                
                StringBuilder sb = new StringBuilder();
                sb.append(LocalDateTime.now());
                sb.append(" ");
                sb.append(m.getAuthor().getUsername());
                sb.append(": ");
                sb.append(m.getContent());
                String lm = sb.toString();
                
            try{
                switch(l){
                    case FILE:
                        try{
                            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true))); 
                            out.println(lm);
                            out.flush();
                        }catch (IOException e) {
                            System.err.println(e);
                        }
                     break;
                        
                    case CHANNEL:
                        JDA c = DisBot.getClient();
                        
                        if(logchannel != null){
                            c.getTextChannelById(logchannel.getId()).sendMessage(lm);
                        }else{
                            s.getPrivateChannel().sendMessage(lm);
                        }
                        
                     break;
                }
            }catch(Exception ex){
                DisBot.l.log(Level.SEVERE, ex.getMessage());
            }
                    
            }
        }
    }
    
    
    
}
