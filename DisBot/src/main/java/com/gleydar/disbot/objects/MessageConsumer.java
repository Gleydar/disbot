/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.objects;

import com.gleydar.disbot.events.MessageHandler;
import net.dv8tion.jda.entities.Message;

/**
 *
 * @author Antimonyy
 */

public abstract class MessageConsumer {
    
    public MessageConsumer(){
        register();
    }

    public abstract void handle(Message m);
    
    public final void register(){
        MessageHandler.registerConsumer(this);
    }
    
}
