/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.objects;

import com.gleydar.disbot.DisBot;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.HttpURLConnection;
import java.net.URL;

import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.MessageChannel;

import org.json.JSONObject;

/**
 *
 * @author Antimonyy
 */

public class CleverBot extends MessageConsumer{

    static MessageChannel mc = null;
    static String nick;
    
    public CleverBot(MessageChannel mh, String n){
        mc = mh;
        nick = n;
    }
    
    @Override
    public void handle(Message m) {
        try {
            if(m.getChannel().equals(mc)){
                if(!m.getAuthor().getId().equalsIgnoreCase(DisBot.getClient().getSelfInfo().getId())){
                    JSONObject o = new JSONObject();
                    o.put("user", ConfigUtil.getStringValue("cleverbotio-user")).put("key", ConfigUtil.getStringValue("cleverbotio-apikey")).put("nick", nick)
                            .put("text", m.getContent());
                    
                    HttpResponse<JsonNode> r = Unirest.post("https://cleverbot.io/1.0/ask").header("Content-Type", "application/json").body(o).asJson();
                    try{
                        m.getChannel().sendMessage(r.getBody().getObject().getString("response"));
                    }catch(Exception e){
                        m.getChannel().sendMessage("Sorry, but there was an error :c");
                    }
                }
            }
        } catch (Exception ex) {
            m.getChannel().sendMessage("Sorry, there has been an exception :c");
            ex.printStackTrace();
        }
    }
    
    public static void setChannel(MessageChannel mh){
        System.out.println(mh);
        mc = mh;
    }
    

}
