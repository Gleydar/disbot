/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.events;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.DisUser;
import com.gleydar.disbot.objects.MessageConsumer;
import com.gleydar.disbot.objects.Module;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.User;
import net.dv8tion.jda.events.message.MessageReceivedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;

/**
 *
 * @author Antimonyy
 */

public class MessageHandler extends ListenerAdapter{
    
    private static ArrayList<MessageConsumer> consumers = new ArrayList<>();
    private static HashMap<String, Module> modules = new HashMap<>();
    private static final File LOG = new File("commands.log");
    
    @Override
    public void onMessageReceived(MessageReceivedEvent e){
        
        Message m = e.getMessage();
        JDA c = DisBot.getClient();
        

        for(MessageConsumer mc : consumers){
            mc.handle(m);
        }
        
        if(e.isPrivate() || containsUser(m)){
            String[] args;
            String l;
            String[] s;
            
            if(e.isPrivate()){  
                s = m.getContent().split(" ");
            }else{
                s = m.getContent().replace("@" + c.getSelfInfo().getUsername() + " ", "").split(" ");
            }
            
            l = s[0];
            args = new String[s.length - 1];
              
            for(int i = 0; i < s.length - 1; i++){
                args[i] = s[i+1];
            }
                
            if(modules.containsKey(l)){
                
                PrintWriter out = null;
                try {
                    out = new PrintWriter(new BufferedWriter(new FileWriter(LOG, true)));
                } catch (IOException ex) {
                    Logger.getLogger(MessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                StringBuilder sb = new StringBuilder().append(LocalDateTime.now()).append(" ").append(m.getAuthor()).append(" used command ").append(l).append(" with Arguments ")
                                .append(Arrays.toString(args));
                
                if(DisUser.getPermissionLevel(e.getAuthor().getId()) >= modules.get(l).getPermissionLevel()){
                    try{
                        modules.get(l).execute(args, m);
                    }catch(Exception ex) {
                        m.getChannel().sendMessage("You broke me :c I have told my dev about it");
                        sb.append(" and returned an error. ").append(ex.getClass().getName()).append(" Stack Trace:").append(Arrays.toString(ex.getStackTrace()));
                        sb.append(" Message: ").append(ex.getMessage());
                    }
                }else{
                    e.getChannel().sendMessage("@" + e.getAuthor().getUsername() + " sorry, but you can't do that");
                    sb.append(" and was denied because of missing permissions");
                }
                
                out.println(sb.toString());
                out.flush();
            }
        }
    }
    
    public boolean containsUser(Message m){
        for(User u : m.getMentionedUsers()){
            if(u.getUsername().equals(DisBot.getClient().getSelfInfo().getUsername())) return true; 
        }
        return false;
    }
    
    public static void registerConsumer(MessageConsumer mc){
        if(!consumers.contains(mc)){
            consumers.add(mc);
        }
    }
    
    public static void registerModule(Module m){
        String l = m.getLabel();
        
        if(!modules.containsKey(l)){
            modules.put(l, m);
        }

    }
    
    public static void unregisterModule(Module m){
        String l = m.getLabel();
        
        if(modules.containsKey(l)){
            modules.remove(l);
        }
    }


}
