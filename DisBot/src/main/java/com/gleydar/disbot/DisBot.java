/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */
package com.gleydar.disbot;

import com.gleydar.disbot.objects.ConfigUtil;
import com.gleydar.disbot.events.MessageHandler;
import com.gleydar.disbot.events.StartEvent;
import com.gleydar.disbot.modules.ChatLoggerHandler;
import com.gleydar.disbot.modules.Clever;
import com.gleydar.disbot.modules.Help;
import com.gleydar.disbot.modules.History;
import com.gleydar.disbot.modules.Imgur;
import com.gleydar.disbot.modules.Info;
import com.gleydar.disbot.modules.Invite;
import com.gleydar.disbot.modules.Purge;
import com.gleydar.disbot.modules.Reddit;
import com.gleydar.disbot.modules.SelfInfo;
import com.gleydar.disbot.modules.WolframAlpha;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.JDABuilder;

/**
 *
 * @author Konstantin
 */
public class DisBot {
    
    public static final Logger l = Logger.getLogger("DisBot");
    private static JDA client;
    private static String token;
    public static String waID;
    public static String imgurID;
    public static String discordID;
    public static Random r = new Random();
    
    public static void main(String[] args) {
        try {
            token = ConfigUtil.getStringValue("bot-token");
            waID = ConfigUtil.getStringValue("wolframalpha-appid");
            imgurID = ConfigUtil.getStringValue("imgur-appid");
            discordID = ConfigUtil.getStringValue("discord-appid");
            manageStart();
            registerModules();
        } catch (Exception ex) {
           l.log(Level.SEVERE, "DisBot", ex);
        }
    }
    
    private static void manageStart() {
        JDABuilder b = new JDABuilder().setBotToken(token);
        try {
            client = registerEvents(b).buildBlocking();
        } catch (Exception ex) {
            Logger.getLogger(DisBot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static JDABuilder registerEvents(JDABuilder b){
        b.addListener(new MessageHandler());
        b.addListener(new StartEvent());
        return b;
    }
    
    private static void registerModules(){
        ChatLoggerHandler clh = new ChatLoggerHandler();
        Invite i = new Invite();
        Help h = new Help();
        SelfInfo sf = new SelfInfo();
        Info in = new Info();
        WolframAlpha wa = new WolframAlpha();
        Imgur im = new Imgur();
        Reddit rd = new Reddit();
        Purge pg = new Purge();
        History hi = new History();
        Clever cl = new Clever();
    }
    
    public static JDA getClient(){
        return client;
    }
    
}
