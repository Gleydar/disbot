/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */
package com.gleydar.disbot.modules;

import com.gleydar.disbot.objects.Module;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import net.dv8tion.jda.MessageHistory;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.TextChannel;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Antimonyy
 */
public class History extends Module {

    @Override
    public void execute(String[] args, Message m) throws Exception {
        StringBuilder text = new StringBuilder();
        boolean normal = true;
        if (!m.isPrivate()) {
            try {
                TextChannel tc = m.getMentionedChannels().get(0);
                MessageHistory mh = new MessageHistory(tc);
                if (args.length >= 2) {
                    try {
                        int me = Integer.parseInt(args[1]);

                        if (mh.equals(m.getChannel())) {
                            mh.retrieve(me + 1);
                        } else {
                            mh.retrieve(me);
                        }
                    } catch (NumberFormatException ne) {
                        m.getChannel().sendMessage("Sorry, but that has to be a number.");
                        return;
                    }
                } else {
                    mh.retrieveAll();
                }
                if (mh.equals(m.getChannel())) {
                    mh.getRecent().remove(0);
                }
                //Determines the format of the paste, weather it is backwards or forwards
                if (normal) {
                    for (int i = mh.getRecent().size() - 1; i > 0; i--) {
                        Message mx = mh.getRecent().get(i);
                        text.append("[").append(mx.getTime().getDayOfMonth()).append(".").append(mx.getTime().getMonth().ordinal()).append(".").append(mx.getTime().getYear()).append(" - ");
                        text.append(mx.getTime().toLocalTime()).append("] ").append("<").append(mx.getAuthor().getUsername()).append(" (").append(mx.getAuthor().getId()).append(")> ");
                        text.append(mx.getContent());
                        text.append("\n");
                    }
                } else {
                    for (Message mx : mh.getRecent()) {
                        text.append("[").append(mx.getTime().getDayOfMonth()).append(".").append(mx.getTime().getMonth().ordinal()).append(".").append(mx.getTime().getYear()).append(" - ");
                        text.append(mx.getTime().toLocalTime()).append("] ").append("<").append(mx.getAuthor().getUsername()).append(" (").append(mx.getAuthor().getId()).append(")> ");
                        text.append(mx.getContent()).append("\n");
                    }
                }
            } catch (IndexOutOfBoundsException ie) {
                m.getChannel().sendMessage("You have to mention a channel for this command to work!");
                return;
            }
        }
        
        //Not really random, I know, but it does what it is supposed to. Expires after 20min anyways.
        String password = RandomStringUtils.randomAlphanumeric(6);
        String content = "text=" + URLEncoder.encode(text.toString().replace("`", ""), "UTF-8") +
               "&password=" + URLEncoder.encode(password, "UTF-8") + "&expire=20m&lang=irc";
        URL url = new URL ("https://www.ghostbin.com/paste/new");
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoOutput (true);
            con.setUseCaches (false);
            con.setRequestProperty("charset", "utf-8");
            con.setRequestProperty( "Content-Length", Integer.toString(content.length()));
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setInstanceFollowRedirects(false);
        con.connect();
        DataOutputStream printout;
    
        printout = new DataOutputStream(con.getOutputStream());
        printout.writeBytes(content);
        printout.flush();
        printout.close();
        
        m.getChannel().sendMessage("Your Log will be aviable for 20 minutes at <https://www.ghostbin.com"+con.getHeaderField("Location")+"> with the password `" + password + "`");
    }

    @Override 
    public int getPermissionLevel() {
        return 3;
    }

    @Override
    public String getLabel() {
        return "history";
    }

}
