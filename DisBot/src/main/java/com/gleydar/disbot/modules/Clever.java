/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.objects.CleverBot;
import com.gleydar.disbot.objects.ConfigUtil;
import com.gleydar.disbot.objects.Module;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import net.dv8tion.jda.entities.Message;
import org.json.JSONObject;

/**
 *
 * @author Antimonyy
 */

public class Clever extends Module{

    @Override
    public void execute(String[] args, Message m) throws Exception {
        try{
            JSONObject o = new JSONObject();
                o.put("user", ConfigUtil.getStringValue("cleverbotio-user")).put("key", ConfigUtil.getStringValue("cleverbotio-apikey"));
            HttpResponse<JsonNode> r = Unirest.post("https://cleverbot.io/1.0/create").header("Content-Type", "application/json").body(o).asJson();
            CleverBot cleverBot = new CleverBot(m.getMentionedChannels().get(0), r.getBody().getObject().getString("nick"));
            m.getChannel().sendMessage("I will monitor the channel " + m.getMentionedChannels().get(0).getAsMention());
        }catch(IndexOutOfBoundsException e){
            m.getChannel().sendMessage("Sorry, I'm doing my best, but you have to mention a channel for this to work!");
        }
    }

    @Override
    public int getPermissionLevel() {
        return 9;
    }

    @Override
    public String getLabel() {
       return "clever";
    }

}
