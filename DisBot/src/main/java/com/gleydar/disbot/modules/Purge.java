/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.Module;
import net.dv8tion.jda.MessageHistory;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.exceptions.PermissionException;

/**
 *
 * @author Antimonyy
 */

public class Purge extends Module{

    @Override
    public void execute(String[] args, Message m) throws Exception {
        if(!m.isPrivate()){
            switch(args.length){
                case 0:
                    try{
                        MessageHistory mh = new MessageHistory(m.getChannel());
                        mh.retrieveAll();
                        for(Message me : mh.getRecent()){
                            me.deleteMessage();
                        }
                    }catch(Exception e){
                        m.getChannel().sendMessage("Sorry, I can't do that :C");
                    }
                    break;
                case 1:
                    try{
                        int i = Integer.parseInt(args[0]);
                        MessageHistory mh = new MessageHistory(m.getChannel());
                        mh.retrieve(i+1);
                        for(Message me : mh.getRecent()){
                            me.deleteMessage();
                        }
                    }catch(NumberFormatException ex){
                        m.getChannel().sendMessage("Sorry, that is not a valid number.");
                    }catch(PermissionException es){
                        int i = Integer.parseInt(args[0]);
                        MessageHistory mh = new MessageHistory(m.getChannel());
                        mh.retrieve();
                        for(Message me : mh.getRecent()){
                            if(me.getAuthor().getJDA() == DisBot.getClient()){
                                try{
                                    me.deleteMessage();
                                    i--;
                                    if(i <= 0){
                                        break;
                                    }
                                }catch(Exception e){
                                                
                                }
                            }
                        }
                    }
                    break;
            }
        }else{
            m.getChannel().sendMessage("Sorry, you can't do that in a private channel :)");
        }
    }

    @Override
    public int getPermissionLevel() {
        return 5;
    }

    @Override
    public String getLabel() {
        return "purge";
    }

}
