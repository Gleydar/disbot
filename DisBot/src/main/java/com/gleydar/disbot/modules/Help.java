/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.Module;
import net.dv8tion.jda.entities.Message;

/**
 *
 * @author Antimonyy
 */

public class Help extends Module{

    @Override
    public String getLabel() {
        return "help";
    }

    @Override
    public void execute(String[] args, Message m) {
            m.getChannel().sendMessage("Hi! I am DisBot :smile:");
            m.getChannel().sendMessage("I was made by Gleydar and run on JDA version `3.0.2`");
            m.getChannel().sendMessage("To see the commands you can use, please use `commands`");
    }
    
    @Override
    public int getPermissionLevel(){
        return 0;
    }

    
}
