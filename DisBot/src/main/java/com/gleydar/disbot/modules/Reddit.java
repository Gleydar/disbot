/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.objects.Module;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import java.util.Arrays;
import net.dv8tion.jda.entities.Message;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Antimonyy
 */

public class Reddit extends Module{

    @Override
    public void execute(String[] args, Message m) throws Exception{
        Thread thread;
        thread = new Thread(){
            @Override
            public synchronized void run(){
                try{
                    String subreddit = "random";
                    if(args.length >= 1){
                        subreddit = args[0];
                    }
                    HttpResponse<JsonNode> r = Unirest.get("https://www.reddit.com/r/" + subreddit + "/random.json")
                            .header("User-Agent", "discord:com.gleydar.disbot:0.1DEV (by /u/gleydar)").asJson();
                    JSONArray a = new JSONArray(r.getBody().getArray());
                    //Fucking hell, WHY REDDIT WHY
                    JSONObject o = a.getJSONObject(0).getJSONObject("data").getJSONArray("children").getJSONObject(0).getJSONObject("data");
                    StringBuilder message = new StringBuilder();

                        message.append("Here is your random post out of /r/").append(subreddit).append(" with the title: `").append(o.getString("title")).append("`\n");
                        String url_i = o.getString("url");
                        if(url_i.contains("imgur") && url_i.contains("gifv")){
                            url_i = url_i.replace("gifv", "gif");
                        }
                        message.append("<").append(url_i).append(">").append("\n");
                        
                        if(o.getString("selftext").length() > 1){
                            message.append("```").append(o.getString("selftext").replace("https://www.reddit.com", ""));
                            while(message.length() + 3 > 2000){
                                int b = message.substring(0, 1996).lastIndexOf(" ");
                                m.getChannel().sendMessage(message.substring(0, b) + "```");
                                message.delete(0, b);
                                message.insert(0, "```");
                                this.wait(10);
                            }
                            m.getChannel().sendMessage(message.append("```").toString());
                        }else{
                            m.getChannel().sendMessage(message.toString());
                        }
                        
                }catch(Exception e){
                    m.getChannel().sendMessage("Sorry, but I encountered an error while doing that :c Does the specified subreddit exist?");
                    System.out.println(e.getMessage());
                    System.out.println(Arrays.toString(e.getStackTrace()));
                }
                
            }
        };
        thread.start();
    }

    @Override
    public int getPermissionLevel() {
        return 3;
    }

    @Override
    public String getLabel() {
        return "reddit";
    }

}
