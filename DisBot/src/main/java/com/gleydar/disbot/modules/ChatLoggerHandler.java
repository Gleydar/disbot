/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.EasyObjects;
import com.gleydar.disbot.objects.ChatLogger;
import com.gleydar.disbot.objects.LogDestination;

import com.gleydar.disbot.objects.Module;
import java.io.File;
import java.util.ArrayList;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.MessageBuilder;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.TextChannel;
import net.dv8tion.jda.entities.User;

/**
 *
 * @author Antimonyy
 */

public class ChatLoggerHandler extends Module{
    
    public ChatLoggerHandler(){
        
    }

    private static final ArrayList<ChatLogger> loggers = new ArrayList<>();

    @Override
    public String getLabel() {
        return "log";
    }

    @Override
    public void execute(String[] args, Message m) throws Exception{
        User sender = m.getAuthor();
        JDA c = m.getJDA();
        ChatLogger cl;
        TextChannel tc;
        TextChannel lc = null;
         
        if(c.getTextChannelById(args[0]) != null){
            tc = c.getTextChannelById(args[0]);
            if(args.length == 4 && c.getTextChannelById(args[3]) != null){
                lc = c.getTextChannelById(args[3]);
            }else if(c.getTextChannelsByName(args[3]).size() == 1){
                lc = c.getTextChannelsByName(args[3]).get(0);
            }
        }else{
            if(c.getTextChannelsByName(args[0]).size() == 1){
                tc = c.getTextChannelsByName(args[0]).get(0);
            }else{
                sendMessage(m, args[0], null);
                return;
            }
        }
            
        switch(args.length){
            case 2: 
                for(ChatLogger cg : loggers){
                    if(cg.getChannel().equals(tc)){
                        cg.stop();
                        
                        if(cg.getDestination().equals(LogDestination.FILE)){
                            File f = new File(cg.getChannel().getName()+".log");
                            sender.getPrivateChannel().sendFileAsync(f, new MessageBuilder().appendString("Here is your logfile :)").build(), null);
                        }
                            sender.getPrivateChannel().sendMessage("Stopped the logger for channel " + args[0]);
                    }
                    return;
                }
                case 3:
                    switch (args[2]) {
                        case "file":
                            cl = new ChatLogger(tc, sender, LogDestination.FILE, null);
                            sender.getPrivateChannel().sendMessage("Hey, I started your logger on channel " + tc.getName() + " to a file");
                         break;
                        case "channel":
                                sender.getPrivateChannel().sendMessage("Sorry, but I dont know what channel to log to :/");
                         break;
                        default: 
                            sender.getPrivateChannel().sendMessage("Sorry, but those arn't valid arguments :(");
                         break;
                    }
                    
                    break;
                    
                case 4:
                    if(c.getTextChannelsByName(args[3]).size() > 0){
                        cl = new ChatLogger(tc, sender, LogDestination.CHANNEL, lc);
                    }else{
                        sender.getPrivateChannel().sendMessage("Apparently there are multiple channels with that name. Please use its ID.");
                    }
                    
                    break;
                default: sender.getPrivateChannel().sendMessage("Sorry, but those arn't valid arguments :(");
                    
            }
            
    }
    
    private static void sendMessage(Message m, String id, String id2){
        JDA c = DisBot.getClient();
        if(EasyObjects.getChannelByName(id).size() > 1){
            m.getAuthor().getPrivateChannel().sendMessage("Detected multiple channels with the same name:");
            for(TextChannel ch : c.getTextChannelsByName(id)){
                m.getAuthor().getPrivateChannel().sendMessage(id + " on server " + ch.getGuild().getName() + " with ID " + ch.getId());
            }
        }
        if(id2 != null && EasyObjects.getChannelByName(id2).size() > 1){
            m.getAuthor().getPrivateChannel().sendMessage("Detected multiple channels to log to with the same name:");
            for(TextChannel ch : EasyObjects.getChannelByName(id2)){
                m.getAuthor().getPrivateChannel().sendMessage(id2 + " on server " + ch.getGuild().getName() + " with ID " + ch.getId());
            }
        }
    }
    
    @Override
    public int getPermissionLevel(){
        return 4;
    }
    
    public static ArrayList<ChatLogger> listLoggers(){
        return loggers;
    }
    
    /*
    * Adds a logger to the list
    */
    public static void registerNewLogger(ChatLogger l){
        if(!loggers.contains(l)){
            loggers.add(l);
        }
    }
    
    /*
    * Removes a logger
    */
    public static void removeLogger(ChatLogger l){
        if(loggers.contains(l)){
            loggers.remove(l);
        }
    }
      
}
