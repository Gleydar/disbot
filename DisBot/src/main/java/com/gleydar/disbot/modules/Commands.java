/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.objects.Module;
import net.dv8tion.jda.entities.Message;

/**
 *
 * @author Antimonyy
 */

public class Commands extends Module{

    @Override
    public void execute(String[] args, Message m) throws Exception {
        
    }

    @Override
    public int getPermissionLevel() {
        return 0;
    }

    @Override
    public String getLabel() {
        return "commands";
    }

}
