/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.Module;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.Role;
import net.dv8tion.jda.entities.TextChannel;
import net.dv8tion.jda.entities.User;

/**
 *
 * @author Antimonyy
 */

public class Info extends Module{

    @Override
    public void execute(String[] args, Message m) throws Exception {
        JDA c = DisBot.getClient();
        StringBuilder r = new StringBuilder();
        switch(args[0].toLowerCase()){
            case "user":
                
                if(args.length == 2){
                    User u = null;
                    
                    try{
                        u = c.getUserById(args[1]);
                        u.getDiscriminator();
                    }catch(Exception e){
                        if(c.getUsersByName(args[1]).size() == 1){
                            u = c.getUsersByName(args[1]).get(0);
                        }else{
                            if(!m.isPrivate()){
                                c.getTextChannelById(m.getChannelId()).sendMessage("There are multiple users with that name: ");
                                for(User us : c.getUsersByName(args[1])){
                                    c.getTextChannelById(m.getChannelId()).sendMessage("`" + us.getUsername() + "` with ID `" + us.getId() + "`");
                                }
                            }else{
                                c.getPrivateChannelById(m.getChannelId()).sendMessage("There are multiple users with that name: ");
                                for(User us : c.getUsersByName(args[1])){
                                    c.getPrivateChannelById(m.getChannelId()).sendMessage("`" + us.getUsername() + "` with ID `" + us.getId() + "`");
                                }
                            }
                        }
                    }    
                    
                    if(u != null){
                        r.append("User `").append(u.getUsername()).append("` with ID `").append(u.getId()).append("` ");
                        
                        if(!m.isPrivate()){
                            r.append("has roles: ");
                            for(Role ro : c.getTextChannelById(m.getChannelId()).getGuild().getRolesForUser(u)){
                                r.append("`").append(ro.getName()).append("` ");
                            }
                            c.getTextChannelById(m.getChannelId()).sendMessage(r.toString());
                            r.delete(0, r.length());
                        }else{
                            c.getPrivateChannelById(m.getChannelId()).sendMessage(r.toString());
                            r.delete(0, r.length());
                        }
                    }
                    
                }else{
                    m.getAuthor().getPrivateChannel().sendMessage("Sorry, those are not valid arguments :c");
                }
                
                break;
            case "channel":
                if(args.length == 1){
                    if(!m.isPrivate()){
                        r.append("`").append(c.getTextChannelById(m.getChannelId()).getName()).append("` with the ID ` ").append(m.getChannelId());
                        r.append("` has topic `").append(c.getTextChannelById(m.getChannelId()).getTopic()).append("`");
                        c.getTextChannelById(m.getChannelId()).sendMessage(r.toString());
                    }else{
                        m.getAuthor().getPrivateChannel().sendMessage("Sorry, that doesnt work with private channels!");
                    }
                }else{
                    try{
                        r.append("Channel ").append(c.getTextChannelById(args[1]).getName()).append(" with ID ").append(args[1]).append(" is on Server ").append(c.getTextChannelById(args[1]).getGuild());
                        if(!m.isPrivate()){
                            c.getTextChannelById(m.getChannelId()).sendMessage(r.toString());
                            r.delete(0, r.length());
                        }else{
                            m.getAuthor().getPrivateChannel().sendMessage(r.toString());
                            r.delete(0, r.length());
                        }
                    }catch(Exception e){
                    if(c.getTextChannelsByName(args[1]).size() > 1){
                        r.append("`").append(c.getTextChannelsByName(args[1]).get(0)).append("` with the ID ` ").append(c.getTextChannelsByName(args[1]).get(0).getId());
                        if(c.getTextChannelsByName(args[1]).get(0).getTopic().length() > 0){
                            r.append("` has topic `").append(c.getTextChannelsByName(args[1]).get(0).getTopic()).append("`");
                        }else{
                            r.append("` has no topic.");
                        }
                        c.getTextChannelById(m.getChannelId()).sendMessage(r.toString());
                    }else{
                        for(TextChannel tc : c.getTextChannelsByName(args[1])){
                            r.append("`").append(tc).append("` with the ID ` ").append(tc.getId());
                            if(tc.getTopic().length() > 0){
                                r.append("` has topic `").append(tc.getTopic()).append("`");
                            }else{
                                r.append("` has no topic.");
                            }
                            c.getTextChannelById(m.getChannelId()).sendMessage(r.toString());
                            r.delete(0, r.length());
                        }
                    }
                    }
                }
                break;
            case "guild":
                try{
                    r.append(c.getTextChannelById(m.getChannelId()).getGuild().getId());
                    r.append(c.getGuildById(args[1]));
                    r.append(c.getUserById(c.getGuildById(args[1]).getOwnerId()));
                for(User u : c.getGuildById(args[1]).getUsers()){
                    r.append(u.getUsername());
                }
                }finally{
                    m.getAuthor().getPrivateChannel().sendMessage(r.toString());
                }
        }
    }

    @Override
    public int getPermissionLevel() {
        return 1;
    }

    @Override
    public String getLabel() {
        return "info";
    }

}
