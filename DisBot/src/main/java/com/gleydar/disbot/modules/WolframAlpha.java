/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.Module;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import net.dv8tion.jda.entities.Message;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

/**
 *
 * @author Antimonyy
 */

public class WolframAlpha extends Module{

    private final String ID = DisBot.waID;
    
    @Override
    public void execute(String[] args, Message m) throws Exception {
        switch(args[0]){
            default: 
                String query = "";
                for (String s : args){
                    query = query + s + "%20";
                }
        
                String url = "http://api.wolframalpha.com/v2/query?input="+query+"&appid="+ID;
                SAXBuilder builder = new SAXBuilder();
                StringBuilder message = new StringBuilder();
                message.append("For query `").append(query.replace("%20", " ")).append("` WolframAlpha returned as most likely result the type `");
	
                try {
                    Document document = (Document) builder.build(new InputSource(new URL(url).openStream()));
                    Element rootNode = document.getRootElement();
                    List<Element> list = rootNode.getChildren("pod");
                    
                    boolean found = false;
                    for(Element e : list){
                        elementloop:
                        switch(e.getAttributeValue("title")){
                            case "Notable Facts":
                                message.append("Notable facts` (prepare) ```").append(e.getChild("subpod").getChildText("plaintext")).append("```");
                            case "Decimal approximation":
                            case "Result":
                            case "Basic Definition":
                                if(e.getChild("subpod").getChildText("plaintext").length() > 0){
                                    message.append(e.getAttributeValue("title")).append("` with the value `").append(e.getChild("subpod").getChildText("plaintext")).append("`");
                                }else{
                                    message.append("Picture` with the URL: ").append(e.getChild("subpod").getChild("img").getAttributeValue("src"));
                                }
                                found = true;
                                break elementloop;
                            default: 
                                
                        }
                    }
                    if(!found){
                        try{
                            Element e = list.get(1);
                            message.append(e.getAttributeValue("title"));
                        
                            if(e.getChild("subpod").getChildText("plaintext").length() > 0){
                                message.append("` with the value ```").append(e.getChild("subpod").getChildText("plaintext")).append("```");
                            }else if(e.getChild("subpod").getChild("img").getAttributeValue("src").length() > 0){
                                message.append("` with the URL: ").append(e.getChild("subpod").getChild("img").getAttributeValue("src"));
                                try{
                                    if(!e.getChild("infos").getAttributeValue("count").equals(0)){
                                        message.append(" further Infos at ").append(e.getChild("infos").getChild("info").getChild("link").getAttributeValue("url"));
                                    }
                                }catch(Exception ec){
                                    
                                }
                            }else{
                                message.append("` with no further value provided (that I can process)");
                            }
                        }catch(IndexOutOfBoundsException e){
                            message.append("Nothing`. It literally couldn't handle the request.");
                        }
                    }
                }catch (IOException io) {
                    System.out.println(io.getMessage());
                }
                
                if(!m.isPrivate()){
                    DisBot.getClient().getTextChannelById(m.getChannelId()).sendMessage(message.toString());
                }else{
                    m.getAuthor().getPrivateChannel().sendMessage(message.toString());
                }
            break;
        }
    }

    @Override
    public int getPermissionLevel() {
        return 3;
    }

    @Override
    public String getLabel() {
        return("wa");
    }

}
