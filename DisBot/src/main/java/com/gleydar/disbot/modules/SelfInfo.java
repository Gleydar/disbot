/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.Module;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import javax.imageio.ImageIO;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.OnlineStatus;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.utils.AvatarUtil;

/**
 *
 * @author Antimonyy
 */

public class SelfInfo extends Module{

    @Override
    public void execute(String[] args, Message m) throws Exception {
        JDA c = DisBot.getClient();
        switch(args[0].toLowerCase()){
            case "avatar":
                    URL url = new URL(args[1]);
                    BufferedImage img = ImageIO.read(url);
                    File avatar = new File("ava.jpg");
                    if(avatar.exists()){
                        avatar.delete();
                    }
                    ImageIO.write(img, "jpg", avatar);

                    c.getAccountManager().setAvatar(AvatarUtil.getAvatar(avatar)).update();
                break;
            case "name":
                String n = "";
                for(int i = 1; i < args.length; i++){
                        if(i == 1){
                            n = args[1];
                        }else{
                            n = n + " " + args[i];
                        }
                    }
                c.getAccountManager().setUsername(n).update();
                break;
            case "status":
                String s = "";
                    for(int i = 1; i < args.length; i++){
                        s = s + " " + args[i];
                    }
                c.getAccountManager().setGame(s);
                
                break;
            case "idle":
                if(c.getSelfInfo().getOnlineStatus().equals(OnlineStatus.ONLINE)){
                    c.getAccountManager().setIdle(true);
                }else{
                    c.getAccountManager().setIdle(false);
                }
                break;
            default:
                m.getAuthor().getPrivateChannel().sendMessage("Sorry, unknown arguments :/");
                break;
        }
    }

    @Override
    public int getPermissionLevel() {
        return 9999;
    }

    @Override
    public String getLabel() {
        return "selfinfo";
    }
    

}
