/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.Module;
import net.dv8tion.jda.Permission;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.utils.ApplicationUtil;

/**
 *
 * @author Antimonyy
 */

public class Invite extends Module{
    
    @Override
    public String getLabel() {
        return "invite";
    }
    
    @Override
    public void execute(String[] args, Message m) throws Exception{
        m.getChannel().sendMessage("Hello! I'm " + DisBot.getClient().getSelfInfo().getUsername() + "! If you are a server admin and you want to add me to your server, here is your link:");
        m.getChannel().sendMessage(ApplicationUtil.getAuthInvite(DisBot.discordID, Permission.getFromOffset(66321471)));
        m.getChannel().sendMessage("Just give or revoke all the permissions you want me to have :)");
    }
    
    @Override
    public int getPermissionLevel(){
        return 0;
    }

}
