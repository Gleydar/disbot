/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot.modules;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.objects.Module;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import net.dv8tion.jda.entities.Message;
import org.json.JSONObject;

/**
 *
 * @author Antimonyy
 */

public class Imgur extends Module{

    @Override
    public void execute(String[] args, Message m) throws Exception {
        int page = 0;
        if(args.length == 1){
            try{
                page = Integer.parseInt(args[0]);
            }catch(NumberFormatException e){
                m.getChannel().sendMessage("Sorry, that is not a valid number");
            }
        }
        
        HttpResponse<JsonNode> r = Unirest.get("https://api.imgur.com/3/gallery/hot/viral/" + page + ".json")
            .header("Authorization", "Client-ID " + DisBot.imgurID).header("User-Agent", "discord:com.gleydar.disbot:0.1DEV").asJson();
        
        JSONObject obj = new JSONObject(r.getBody().getObject());
        int i = DisBot.r.nextInt(obj.getJSONArray("data").length());
        JSONObject o = obj.getJSONArray("data").getJSONObject(i);
        
        StringBuilder reply = new StringBuilder("Here is a random ");
        if(o.getBoolean("is_album")){
            reply.append("album off the frontpage for you: ").append(o.getString("link"));
        }else if(o.getBoolean("animated")){
            reply.append("gif off the frontpage for you: ").append(o.getString("link")).append(" with the tile: `").append(o.getString("title")).append("`");
        }else{
            reply.append("image off the frontpage for you: ").append(o.getString("link")).append(" with the tile: `").append(o.getString("title")).append("`");
        }
        m.getChannel().sendMessage(reply.toString());
    }

    @Override
    public int getPermissionLevel() {
        return 2;
    }

    @Override
    public String getLabel() {
        return "imgur";
    }

}
