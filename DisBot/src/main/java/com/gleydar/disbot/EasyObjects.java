/*
 *  This project was developed by Antimonyy. All rights reserved 
 *  unless explicitly stated otherwise.
 */

package com.gleydar.disbot;

import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.entities.TextChannel;

/**
 *
 * @author Antimonyy
 */

public class EasyObjects {

    public static List<TextChannel> getChannelByName(String n){
        JDA c = DisBot.getClient();
        List<TextChannel> list = new ArrayList<>();
        try{
            c.getTextChannelById(n).sendMessage("");
            list.add(c.getTextChannelById(n));
            return list;
        }catch(Exception e){
            return c.getTextChannelsByName(n);
        }
    }
    
}
